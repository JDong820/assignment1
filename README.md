EE422C
======

jabba 8 for the University of Texas at Austin, Spring 2015.

I'm not the best XML programmer, so you won't find any POMs here. Long live
make.


Disclaimer
----------

Using any version, modified or original, of the code in this project without
proper licensing is a legal no-no. It was committed here first, so please
avoid getting yourself in trouble. The code's not even that good.


Licensing
---------

Code is released under GPLv3 share-alike.
